# Clone Repository
The Blender Studio Pipeline git repository contains many tools and resources used in deploying and managing a Blender Studio Pipeline. To start our pipeline we will need to clone this repository. To begin open a terminal/command prompt in your homefolder.

```bash 
# Windows & Linux/Mac
cd data # This directory is the root storage that will contain all projects
git lfs install # Ensure git-lfs is installed
git clone https://projects.blender.org/studio/blender-studio-pipeline.git
```